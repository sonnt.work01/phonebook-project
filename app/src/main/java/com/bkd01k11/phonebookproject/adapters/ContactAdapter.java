package com.bkd01k11.phonebookproject.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.bkd01k11.phonebookproject.R;

public class ContactAdapter extends CursorAdapter {

    public TextView contactIcon;
    private Context _context;
    public  String contactName;

    public ContactAdapter(Context context, Cursor c) {
        super(context, c);
        _context = context;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.contact_row, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name =  view.findViewById(R.id.Name);
        TextView email = view.findViewById(R.id.Email);
        TextView phone = view.findViewById(R.id.Phone);
        TextView tag  = view.findViewById(R.id.tagName);

        String Name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        String Email = cursor.getString(cursor.getColumnIndexOrThrow("email"));
        String Phone = cursor.getString(cursor.getColumnIndexOrThrow("phone"));
        String Tag = cursor.getString(cursor.getColumnIndexOrThrow("tag"));

        name.setText(Name);
        email.setText(Email);
        phone.setText(Phone);
        tag.setText(Tag);
    }
}
