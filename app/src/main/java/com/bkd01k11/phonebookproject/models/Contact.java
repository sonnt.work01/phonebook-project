package com.bkd01k11.phonebookproject.models;

public class Contact {
    public int ContactId;
    public String Name;
    public String Phone;
    public String Email;

    public Contact(int contactId, String name, String phone, String email) {
        ContactId = contactId;
        Name = name;
        Phone = phone;
        Email = email;
    }

    public int getContactId() {
        return ContactId;
    }

    public void setContactId(int contactId) {
        ContactId = contactId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}