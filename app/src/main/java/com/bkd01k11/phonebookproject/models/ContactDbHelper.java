package com.bkd01k11.phonebookproject.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ContactDbHelper extends SQLiteOpenHelper {

     private static final String  DATABASE = "phonebook-project-mobile.db";
     private static final String TABLE ="contacts";
     private static final String COLUMN_ID = "_id";
     private static final String COLUMN_NAME = "name";
     private static final String COLUMN_PHONE ="phone";
     private static final String COLUMN_EMAIL ="email";
    private static final String COLUMN_TAG ="tag";

     public ContactDbHelper(Context context) {
        super(context, DATABASE, null, 1);
        SQLiteDatabase db = getWritableDatabase();
    }

     @Override
     public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE contacts " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT,name Text,phone Text UNIQUE, email Text, tag Text)");

    }

     @Override
     public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+TABLE);
        onCreate(db);
    }

     public boolean addContact(String name, String phone, String email, String tag) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, name);
        cv.put(COLUMN_PHONE, phone);
        cv.put(COLUMN_EMAIL, email);
        cv.put(COLUMN_TAG, tag);
        long result = db.insert(TABLE, null, cv);
        if(result == -1){
            return false;
        }
        else{
            return true;
        }
    }

     public Cursor getAllContacts(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM " + TABLE  + " ORDER BY " + COLUMN_NAME + " COLLATE NOCASE ASC",null );
       return data;

     }

     public Cursor getAllContactById(String id){
        SQLiteDatabase db = getWritableDatabase();
        String query ="SELECT * FROM " + TABLE + " WHERE _id = ?";
        Cursor data = db.rawQuery(query, new String[]{id});
        return data;
     }

     public void deleteContact(String contactId){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " +TABLE + " WHERE _id = " +contactId + "");
     }

    public Cursor searchContact(String queryText){
        SQLiteDatabase db = getWritableDatabase();
        String[] args = new String[1];
        args[0] = "%"+queryText+"%";
        String query ="SELECT * FROM contacts WHERE name LIKE ? OR phone LIKE ?";
        Cursor cursor = db.rawQuery(query,args);
        return cursor;
    }

    public void updateContact(String id, String name, String phone, String email, String tag){
      SQLiteDatabase db = getWritableDatabase();
      ContentValues cv = new ContentValues();
      cv.put("name", name);
      cv.put("email", email);
      cv.put("phone",phone);
      cv.put("tag",tag);
      db.update(TABLE,cv,"_id=?",new String[]{id});
    }
}
